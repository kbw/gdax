#include <Poco/URI.h>
#include <Poco/Net/Context.h>
#include "Poco/Net/HTTPSClientSession.h"

#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/WebSocket.h"

#include <date/date.h>
#include <spdlog/spdlog.h>

#include <chrono>
#include <iostream>
#include <vector>

bool isFrameFinished(int flags) {
	return static_cast<bool>(flags & Poco::Net::WebSocket::FRAME_FLAG_FIN);
}

ssize_t recv(Poco::Net::WebSocket& websocket, char* receiveBuffer, size_t receiveBufferLen) {
	ssize_t pos = 0;

	int flags;
	do {
		flags = 0;
		ssize_t nbytes = websocket.receiveFrame(&receiveBuffer[pos], receiveBufferLen - pos - 1, flags);
		pos += nbytes;
	} while (!isFrameFinished(flags) && pos);

	receiveBuffer[pos] = 0;

	return pos;
}

std::string buildRequest(const std::vector<std::string>& productIds) {
	const std::string prefix = R"({"type": "subscribe", "product_ids": [)";
	const std::string suffix = R"(], "channels": ["full"] })";

	std::string str;
	bool first{true};
	for (const std::string& productId : productIds) {
		if (first)
			first = false;
		else
			str += ",";

		str += '"' + productId + '"';
	}

	auto out = prefix + str + suffix;
	return out;
}

std::vector<std::string> instrumentIds(int argc, char* argv[]) {
	std::vector<std::string> out;

	for (int i = 1; i < argc; ++i)
		out.emplace_back(argv[i]);

	return out;
}

const std::string url = "wss://ws-feed.pro.coinbase.com:443/";
constexpr size_t receiveBufferSize = 256 * 1024;
char receiveBuffer[receiveBufferSize];

int main(int argc, char **argv)
try {
	Poco::URI uri(url);
	const auto wsRequest = buildRequest(instrumentIds(argc, argv));

	auto* context = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", "", Poco::Net::Context::VERIFY_NONE, 0, true);
	Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort(), context);

	Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET, uri.getPathAndQuery(), Poco::Net::HTTPMessage::HTTP_1_1);

	Poco::Net::HTTPResponse response;
	Poco::Net::WebSocket websocket(session, request, response);
	websocket.setReceiveTimeout(Poco::Timespan(std::chrono::seconds(0).count(), std::chrono::microseconds(500000).count()));
	websocket.sendFrame(wsRequest.c_str(), static_cast<int>(wsRequest.size()));

	using namespace date;
	std::cout << std::chrono::system_clock::now() << ' ' << "--------" << '\n';
	while (true) {
		recv(websocket, receiveBuffer, sizeof(receiveBuffer));

//		spdlog::info("{}: {}", std::chrono::system_clock::now(), receiveBuffer);
		std::cout << std::chrono::system_clock::now() << ' ' << receiveBuffer << '\n';
	}
}
catch (const std::exception& e) {
	spdlog::error("fatal: {}", e.what());
}
